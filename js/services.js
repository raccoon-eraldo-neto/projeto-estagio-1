(function () {
    const btnServices = document.querySelectorAll('.item-list');
    const text = '-text';

    btnServices.forEach(btnService => {
        btnService.onclick = () => {
            const btnServiceActive = document.querySelector('.item-active');
            btnService.classList.add('item-active');
            if (btnService != btnServiceActive) {
                btnServiceActive.classList.remove('item-active');
            }
            const idbutton = btnService.getAttribute('id');
            const textItem = document.querySelector('#' + idbutton + text);
            const textItemActive = document.querySelector('.text-active');
            textItemActive.classList.add('opacity');
            textItemActive.classList.remove('no-opacity');
            textItemActive.classList.remove('text-active');
            textItemActive.classList.add('display-none');
            textItem.classList.remove('display-none');
            textItem.classList.add('text-active');
            setTimeout(function () {
                textItem.classList.remove('opacity');
                textItem.classList.add('no-opacity');
            }, 300);
        };
    });

})();