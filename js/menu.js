(function () {
    let btnControls = document.querySelectorAll('.icon-menu'),
        menuContainer = document.querySelector('.menu');

        btnControls.forEach(btnControl => {
            btnControl.onclick = () => {
                if (menuContainer.getAttribute('class').includes('open')) {
                    menuContainer.classList.add('menu');
                    menuContainer.classList.remove('open');
                } else {
                    menuContainer.classList.add('menu', 'open');
                }
            };
        });
  
})();


